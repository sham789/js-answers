

**Напишите код программы, которая получает от пользователя слова, разделенные пробельными символами, и выводит: самое длинное слово из исходного файла на отдельной строке, самое короткое слово из исходного текста на отдельной строке.**
```javascript
// Take first input element
const input = document.getElementsByTagName('input')[0]
const getResults = (text) => {
	const splittedText = text.split(' ')
	let theLongest = splittedText[0],
		theSmallest = splittedText[0];

	for (let i = 1; i < splittedText.length; i++) {
		const word = splittedText[i]

		if (word.length > theLongest.length) {
			theLongest = word
		}
		if (word.length < theSmallest.length) {
            theSmallest = word
		}
	}
	
	return [theLongest, theSmallest]
}
console.log(getResults('I like shops that sell biscuits'))
// [ 'biscuits', 'I' ]
```

**Напишите свой собственный аналог метода Array.map (допускается оформить в виде функции, принимающей массив и функцию одного аргумента). Приведите пример применения.**

```javascript
const defaultMap = (arrayItem, arrayIndex, array) => {
	return arrayItem
}
// Функция обратного вызова указана по умолчанию
const arrayMap = (array, callback = defaultMap) => {
	const mapped = []
	for (let i = 0; i < array.length; i++) {
		const mappedItem = callback(array[i], i, array)
		mapped.push(mappedItem)
	}
	return mapped
}

const numbers = [1,2,3,4]
console.log(arrayMap(numbers), x => x*10)
// [10,20,30,40]
```

**Напишите свой собственный аналог метода Array.filter (допускается оформить в виде функции, принимающей массив и функцию одного аргумента и возвращающей новый массив). Приведите пример применения.**

```javascript

// Callback должен возвращать тип значения boolean при вызове
const arrayFilter = (array, callback) => {
	if (!callback) {
		throw new Error('Callback not provided')
	}

	const filtered = []
	for (let i = 0; i < array.length; i++) {
		const item = array[i]
		if (callback(item, i, array)) {
			filtered.push(item)
		}
	}
	return filtered
}
const numbers = [1,2,3,4]
console.log(arrayFilter(numbers, x => x > 2))
// [3,4]
```

**Напишите свой собственный аналог метода Array.reduce (допускается оформить в виде функции, принимающей массив и функцию одного аргумента и возвращающей новый массив). Приведите пример применения.**

```javascript
// Func example
const reduceCallback = (accumulator, arrayItem, arrayIndex, array) => {
	return arrayItem
}

const arrayReduce = (array, reduceCallback, initValue) => {
	if (!reduceCallback) {
		throw new Error('Reduce callback not provided')
	}
	
	let result = initValue

	for (let i = 0; i < array.length; i++) {
		const newResult = reduceCallback(
			result, array[i], i, array
		)
		result = newResult
	}

	return result
}
const numbers = [1,2,3]
console.log(
  arrayReduce(numbers, (acc,iter) => acc+iter, 0)
)
// 6
```

**Напишите свой собственный аналог метода Array.sort (допускается оформить в виде функции, принимающей массив и функцию одного аргумента и возвращающей новый массив). Допускается любой алгоритм сортировки на ваше усмотрение. Не используйте встроенные средства сортировки. Приведите пример применения.**

```javascript
// Пример быстрой сортировки
const quickSort = (array,start, end) => {
	const getPartition = (arr, start, end) => {
		const pivot = arr[end]

		for (let i=start;i<end;i++) {
			if (arr[i]<pivot) {
				const temp = arr[start]
				arr[start] = arr[i]
				arr[i] = temp
				start++
			}
		}

		const temp = arr[start]
		arr[start] = pivot
		arr[end] = temp
		return start
	}
	
	const partition = getPartition(array, start, end)

	if (partition-1>start) {
		quickSort(array, start, partition-1)
	}
	if (partition+1<end) {
		quickSort(array, partition+1, end)
	}
	return array
}

const arr = [1,2,3,50,1,4,12,15]
console.log(quickSort(arr, 0, arr.length - 1))
// [1, 1, 2, 3, 4, 12, 15, 50]
```

**Напишите функцию, возвращающую наибольший и наименьший элементы массива.**

```javascript

const getMinAndMax = (array) => {
	let min = +Infinity, max = -Infinity;

	for (let i = 0; i < array.length; i++) {
		if (array[i] > max) max = array[i];
		if (array[i] < min) min = array[i];
	}
	return [min, max]
}
// Или
const getMinAndMax2 = (array) => {
	const min = Math.min(...array)
	const max = Math.max(...array)
	return [min, max]
}
```

**Напишите код предиката, который проверяет, делится ли целое число на 2 и 3 без остатка. Предусмотрите ситуацию, когда предикату передано значение, не являющееся целым числом, программа в этом случае не должна аварийно завершаться.**

```javascript

const condition = (item) => Number.isFinite(item) && !Number.isNaN(item) ? (
  (item % 2 === 0) && (item % 3 === 0)
) :  false

console.log(condition(0))
// true
console.log(condition('a'))
// false
console.log(condition(2*3))
// true
```

**Напишите определение класса, который предназначен для хранения таблицы размером m×n ячеек и включает в себя методы для: записи значения в указанную ячейку таблицы, чтения значения из указанной ячейки таблицы, получения всех значений заданной строки таблицы в виде списка, получения всех значений заданного столбца таблицы в виде массива. Приведите пример применения.**

```javascript
class Matrix {
	constructor(m, n) {
		this._matrix = this.initMatrix(m, n)
	}

	initMatrix(m, n) {
		const matrix = Array(m).fill(
			Array(n).fill(null)
		)

        return matrix
	}
	
	write(m, n, value) {
		this._matrix[m][n] = value
	}

	readCell(m, n) { return this._matrix[m][n]; }

	readRow(m) { return this._matrix[m]; }
}
const a = new Matrix(10, 5)
// Поставить 10 в вторую ячейку первой строчки
a.write(0,1, 10)
// Поставить 15 в 3 ячейку первой строчки
a.write(0,2, 15)

console.log(a.readRow(0))
// [null, 10, 15, null, null]
```

**Напишите определения классов «счёт в банке», «клиент». Имейте в виду, что один клиент может иметь несколько счетов. Предусмотрите методы для зачисления средств на счет, снятия средств со счета, получения остатка по счету. Напишите функцию для перевода заданной суммы с одного счета на другой. Приведите пример применения в программе.**

```javascript
const banksAvailable = {
	'VTB': 'vtb',
	'SB': 'sberbank'
}

const RUBCurrency = 'RUR'

class BankAccount {
	constructor(name, holder) {
		this.balance = 0
		this.name = name
		this.accountHolder = holder
		// Привязка к одному виду валюты
		this.currency = RUBCurrency
	}

	deposit(amount) {
		if (amount < 0) {
			throw new Error(
			  'Cannot deposit negative amount'
			)
		}
		this.balance += amount;
	}
	
	withdraw(amount) {
		const newBalance = this.balance - amount;
		
		if (amount < 0) {
			throw new Error(
			  'Cannot withdraw negative amount'
			)
		} else if (newBalance < 0) {
			throw new Error(
			  'Not enough balance'
			)
		}
		
		this.balance = newBalance
	}
}

class BankClient {
	constructor(name) {
		this._bankAccounts = {}
		this.name = name
	}

	addBankAccount(bankName) {
		const account = new BankAccount(
			bankName, this.name
		)
		this._bankAccounts[bankName] = account
	}

	getAssetInfo() {
		const res = {}
		const keys = Object.keys(this._bankAccounts)
		for (const key of keys) {
			const val = this._bankAccounts[key]
			res[key] = val.balance
		}
		return res
	}

	transfer(source, dest, amount) {
        this._bankAccounts[dest].deposit(amount)
		this._bankAccounts[source].withdraw(amount)
	}
	
	deposit(bankName, amount) {
		this._bankAccounts[bankName].deposit(amount)
	}
	
	withdraw(bankName, amount) {
		this._bankAccounts[bankName].withdraw(amount)
	}
}

const client = new BankClient('James')
client.addBankAccount(banksAvailable.VTB)
client.addBankAccount(banksAvailable.SB)

console.log(client.getAssetInfo())
// {vtb: 0, sberbank: 0}

client.deposit(banksAvailable.VTB, 1000)
client.deposit(banksAvailable.SB, 50000)

console.log(client.getAssetInfo())
// {vtb: 1000, sberbank: 50000}

// Код ниже вызовет ошибку "Недостаточный баланс"
// На балансе сбербанка только 1000
// client.transfer(banksAvailable.VTB,banksAvailable.SB, 5000)

client.transfer(banksAvailable.VTB,banksAvailable.SB, 1000)

console.log(client.getAssetInfo())
// {vtb: 0, sberbank: 51000}
```


**Напишите код предиката, который принимает массив и возвращает «истину», если в массиве хотя бы один из элементов последовательности встречается в ней более одного раза. Приведите пример применения.**

```javascript
// Есть стандартный вариант #1
const hasDuplicates1 = (array) => {
	const filtered = array.filter(
		(item, i, arr) => arr.indexOf(item) === i
	)
	return array.length !== filtered.length
}
// Можно считать частотность
const hasDuplicates2 = (array) => {
	const refs = {}
	
	for (let i = 0; i < array.length; i++) {
		const val = array[i];
		if (refs[val] === undefined) {
			refs[val] = 0
		}
		if (refs[val] !== undefined) refs[val]++;
		
		if (refs[val] > 1) return true
	}

	return false
}

const arr = [1,1,2,3,4,5]
const arr2 = [1,2,3,4,5]

console.log(hasDuplicates1(arr))
// true
console.log(hasDuplicates1(arr2))
// false

console.log(hasDuplicates2(arr))
// true
console.log(hasDuplicates2(arr2))
// false
```


**Напишите код функции, которая принимает массив и предикат и возвращает «истину», если в массиве хотя бы один из элементов удовлетворяет предикату. Приведите пример применения.**

```javascript
const predicateExample = item => true

const resolvesOnce = (array, predicate = predicateExample) => {
	for (let i = 0; i < array.length; i++) {
		const condition = predicate(array[i]);
		if (condition) return true;
	}
	return false
}
console.log(
  resolvesOnce([1,2,3,4,5], item => item % 2 == 0)
)
// true
console.log(
  resolvesOnce([1,2,3,4,5], item => item < 0)
)
// false
```

**Напишите код функции, которая принимает массив и предикат и возвращает «истину», если в массиве все без исключения элементы удовлетворяют предикату. Приведите пример применения.**

```javascript
const predicateExample = item => true

const resolvesAll = (array, predicate = predicateExample) => {
	for (let i = 0; i < array.length; i++) {
		const condition = predicate(array[i]);
		if (!condition) return false;
	}
	return true
}
console.log(
  resolvesAll([1,2,3,4,5], item => item % 2 == 0)
)
// false
console.log(
  resolvesAll([1,2,3,4,5], item => item > 0)
)
// true
```

**Используя методы встроенного класса «массив», определите функцию, которая удаляет из него самый большой и самый маленький элемент. Допускается формирование нового массива. Приведите пример применения.**

```javascript

const dropMinAndMax = (array) => {
	const max = Math.max(...array)
	const min = Math.min(...array)
	
	// Удалить самый большой элемент
	array.splice(array.indexOf(max), 1)
	// Удалить самый малый элемент
	array.splice(array.indexOf(min), 1)
}

const arr = [10,20,30,40,50,60]
const arr2 = [-1000, 40, 5000, 90000]

dropMinAndMax(arr)
console.log(arr)
// [20, 30, 40, 50]

dropMinAndMax(arr2)
console.log(arr2)
// [40, 5000]
```

**Напишите функцию, принимающую массив чисел и возвращающую среднее арифметическое этих чисел.**

```javascript

const getAverage = (array) => {
	const sum = array.reduce((arr, iter) => arr+iter,0)
	return Math.round(sum / array.length)
}

console.log(getAverage([1,2,3,4,5]))
// 3
console.log(getAverage([10,20,30,40,50]))
// 30
```

**Как на языке Javascript можно реализовать представления множеств, не используя встроенный класс Set. Напишите функции для выполнения основных операций над множествами, представленных предложенным вами способом.**

```javascript
class CustomSet {
	constructor(initValues = []) {
		this.validateParams(initValues)
		// Внутренняя переменная, по соглашению
		// добавляется '_' в начало имени
		this._list = initValues
		// Размер будет обновляться
		// при каждой мутации
		this.updateSize()
	}

	updateSize() {
		this.size = this._list.length
	}
	
	validateParams(params) {
		// Разрешаем только массивы
		if (!Array.isArray(params)) {
			throw new Error(
				'Provided param is not iterable'
			)
		}
	}

	add(value) {
		// Возвращаем новое состояние
		this._list.push(value)
		this.updateSize()
		return this._list
	}

	has(value) {
		for(let i = 0;i < this._list.length;i++) {
			if (value === this._list[i]) {
				return true
			}
		}
		return false
	}

	delete(value) {
		// Поиск и удаление по ссылке
		// Возвращаемое значение -
		// Индикатор успешности операции
		for(let i = 0;i < this._list.length;i++) {
			if (value === this._list[i]) {
				this._list.splice(i, 1)
				this.updateSize()
				return true
			}
		}
		
		return false
	}
}

// Опциально: - поддержка итератора for of
CustomSet.prototype[Symbol.iterator] = function* () {
    for (let i = 0; i < this._list.length; i++) {
        yield [this._list[i], i]
    }
}

const set = new CustomSet([1,2,3])
const set2 = new CustomSet()

// Приходится оборачивать объект в JSON
// т.к. браузер не показывает промежуточное состояние объекта (в рантайме), а только конечное
console.log(JSON.stringify({ set, set2 }))
// {"set":{"_list":[1,2,3],"size":3},"set2":{"_list":[],"size":0}}

set2.add(10)

const obj = { a: 10 }
set2.add(obj)
set.delete(1)

//console.log(set, set2)
console.log(JSON.stringify({ set, set2 }))
// {"set":{"_list":[2,3],"size":2},"set2":{"_list":[10,{"a":10}],"size":2}}

set.delete(2)
// Удаление переменных которые не являются
// примитивами происходит успешно только при наличии ссылке (сохранение в переменной)
set2.delete(obj)
console.log(JSON.stringify({ set, set2 }))
// {"set":{"_list":[3],"size":1},"set2":{"_list":[10],"size":1}}
```

**Используя методы встроенного класса «строка», определите функцию, которая будет проверять равенство двух строк без учета регистра и концевых пробелов. Например, функция должна считать равным строки javascript, JavaScript и JaVaScRiPt.**

```javascript
const equals = (str1, str2) => {
	// Убираем пробелы по концам
	// Приводим к одному регистру и сравниваем
	return (
		str1.trim().toLowerCase() === str2.trim().toLowerCase()
	)
}

console.log(equals('JaVaScRipT ', '    javascript '))
// true
```
